<?php
	include('../includes/header.php');
	include('../includes/navbar.php');
	session_start();
?>

<div class="py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-5">
				<?php include('../includes/message.php')?>
				<div class="card">
					<div class="card-header"><h2>Change password</h2></div>
					<div class="card-body">
						<form action="forgetpasswordDB.php"method ="POST">
							<div class="form-group mb-3">
								<label>Email ID</label></br>
								<input type="email" name="email" placeholder="Enter your mail address">
							</div>
							<div class="form-group mb-3">
								<label>key</label></br>
								<input type="text" name="key" placeholder="Enter KEY">
							</div>
							<div class="form-group mb-3">
								<label>New Password</label></br>
								<input type="password" name="password" placeholder="Enter new password">
							</div>
							<div class="form-group mb-3">
								<label>Confirm New Password</label></br>
								<input type="password" name="cpassword" placeholder="Re-enter new password">
							</div>
							<div class="form-group mb-3">
								<button type = "submit" name ="change_btn"class="btn btn-primary">change password</button>
							</div>
						</form>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
