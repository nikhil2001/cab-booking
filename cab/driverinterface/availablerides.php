<?php
include('../form/formconn.php');
session_start();
$userID = $_SESSION['drivermail'];
// $fromq="SELECT * FROM userrides WHERE completed=0";
// $from = mysqli_query($conn,$fromq);
// $f=mysqli_fetch_assoc($from);
// $latfrom=$f['latfrom'];
// $longfrom =$f['longfrom'];
// $driverq="SELECT * FROM cardriver WHERE email='$userID'";
// $driver = mysqli_query($conn,$driverq);
// $d=mysqli_fetch_assoc($driver);
// $lat= $d['lat'];
// $long = $d['lng'];
// mysqli_free_result($from);
// mysqli_free_result($driver);
?>


<html>
<head>
    <title>Add Map</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
  </head>
  <body onload = "JavaScript:AutoRefresh(2000);">
    <!-- navbar starts -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Nk's Driver-portal</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">view-mail</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
               Account
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#"><?= $userID ?></a></li>
                <form action="logout.php" method="post">
                  <li><button class="dropdown-item" type="submit" name="logoutBtn">Log-out</button></li>
                </form>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- navbar ends -->






  <div class="emailList" style="position: relative;left: 30px;">

      <div class="mail-inbox">
      
      </div>
      <?php include('../includesdriver/message.php');
      function distance($lat1, $lon1, $lat2, $lon2) {

  
          $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          return ($miles * 1.609344);

      }
      
      
      ?>
      <?php 
          $checker="SELECT * FROM cardriver WHERE engaged=0 AND email='$userID'";
          $checker_run = mysqli_query($conn,$checker);
          if(mysqli_num_rows($checker_run)>0){
            $query="SELECT * FROM userrides WHERE completed=0";
            $query_run = mysqli_query($conn,$query);
            if(mysqli_num_rows($query_run)>0){
              foreach($query_run as $row) {

                $fromq="SELECT * FROM userrides WHERE completed=0";
                $from = mysqli_query($conn,$fromq);
                $f=mysqli_fetch_assoc($from);
                $latfrom=$f['latfrom'];
                $longfrom =$f['longfrom'];
                $driverq="SELECT * FROM cardriver WHERE email='$userID'";
                $driver = mysqli_query($conn,$driverq);
                $d=mysqli_fetch_assoc($driver);
                $lat= $d['lat'];
                $long = $d['lng'];
                mysqli_free_result($from);
                mysqli_free_result($driver);


                $distan = distance($lat, $long, $latfrom, $longfrom) ;
                if($distan < 10){
                ?>
                <div class="emailList__list">
                  <div class="emailRow" > 
                    <div class="emailRow__options">
                    <h4 class="userName" ><?= $row['mailid'] ?></h4> 
                    </div>

                    <div class="emailRow__message" style="margin-left:100px;">
                    <h4 style="padding-left: 25px;"> Pick-up : <?= $row['fromloc'] ?></h4>
                    <h4 style="padding-left: 25px;"> Drop : <?= $row['toloc'] ?></h4>
                    </div>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end" style="margin-left: 250px;">
                      <h4 style="padding-right: 15px; padding-top:15px; ">Estimatedfare : <?= $row['fare'] ?></h4>
                      <button class="btn btn-dark btn-sm" type="button" onclick="window.open('enterotp.php','_self')">accept ride</button>
                    </div>
                  </div>
                </div>
                <?php
                }
              }
            }
          }else{
            $_SESSION['message']="You have an un completed ride";
          }
          ?>
    </div>
    <div class="d-grid gap-2 col-4 mx-auto" style="padding-top: 30px; ">
      <button class="btn btn-outline-dark" type="button" onclick="window.open('index.php','_self')">Back</button>
    </div>
<script>
    function AutoRefresh( t ) {
               setTimeout("location.reload(true);", t);
            }
</script>
  </body>
</html>