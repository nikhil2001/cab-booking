<?php
include('../form/formconn.php');
session_start();
$userID = $_SESSION['drivermail'];
?>


<html>
<head>
    <title>Previous rides</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <!-- navbar starts -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Nk's Driver-portal</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
               Account
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#"><?= $userID ?></a></li>
                <form action="logout.php" method="post">
                  <li><button class="dropdown-item" type="submit" name="logoutBtn">Log-out</button></li>
                </form>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- navbar ends -->






  <div class="emailList" style="position: relative;left: 30px;">

      <div class="mail-inbox">
      
      
      <?php 
            $query="SELECT * FROM driverrides WHERE driverid='$userID'";
            $query_run = mysqli_query($conn,$query);
            if(mysqli_num_rows($query_run)>0){
              foreach($query_run as $row) {
                ?>
                <div class="emailList__list">
                  <div class="emailRow" > 
                    <div class="emailRow__options">
                    <h4 class="userName" ><?= $row['userid'] ?></h4> 
                    </div>

                    <div class="emailRow__message" style="margin-left:100px;">
                    <h4 style="padding-left: 10px;"> Pick-up : <?= $row['fromloc'] ?></h4>
                    <h4 style="padding-left: 10px;"> Drop : <?= $row['toloc'] ?></h4>
                    </div>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end" style="margin-left: 250px;">
                      <h4 style="padding-right: 15px; padding-top:15px; ">Fare :  <?= $row['fare'] ?></h4>
                      <h4>drive completed successfully</h4>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
          ?>
    </div>
    </div>
    <div class="d-grid gap-2 col-4 mx-auto" style="padding-top: 30px; ">
      <button class="btn btn-outline-dark" type="button" onclick="window.open('index.php','_self')">Back</button>
    </div>
  </body>
</html>