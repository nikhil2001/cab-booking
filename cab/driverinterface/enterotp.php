<?php
include('../form/formconn.php');
session_start();
$fromq="SELECT * FROM userrides WHERE completed=0";
$from = mysqli_query($conn,$fromq);
$f=mysqli_fetch_assoc($from);
// $userID = $f['mailid'];
// $otp = $f['otp'];
?>


<html>
  <head>
    <title>otp generate</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
    <script type="module" src="./index.js" ></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.8.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.8.1/mapbox-gl.css' rel='stylesheet' />
  </head>
  <body>

  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script>
  <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css" type="text/css">


    <!-- navbar starts -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Nk's cab booking</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Previous rides</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
               Account
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#"><?$f['mailid']?></a></li>
                <form action="logout.php" method="post">
                  <li><button class="dropdown-item" type="submit" name="logoutBtn">Log-out</button></li>
                </form>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- navbar ends -->
<div ><?php include('../includesdriver/message.php')?></div>
    <div class="input-group mb-3"  style="padding-left: 42%;padding-top:50px">
	<form action="otpcheck.php" method="post">
		<input type="text" class="form-control" placeholder="enter otp" name='otp'><br>
		<button class="btn btn-dark" type="submit" style="margin-left: 50px; " name="otpcheck" >start ride</button>
	</form>
    </div>
    <div class="d-grid gap-2 col-4 mx-auto" style="padding-top: 30px; ">
    <button class="btn btn-outline-dark" type="button" onclick="window.open('index.php','_self')">Back</button>
    </div>
  </body>
</html>