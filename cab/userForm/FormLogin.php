<?php
	include('../includes/header.php');
	include('../includes/navbar.php');
	session_start();
?>

<div class="py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-5">
				<?php include('../includes/message.php')?>
				<div class="card">
					<div class="card-header"><h2>Login</h2></div>
					<div class="card-body">
						<form action="FormLoginDB.php"method ="POST">
							<div class="form-group mb-3">
								<label>Email ID</label></br>
								<input type="email" name="email" placeholder="Enter your mail address">
							</div>
							<div class="form-group mb-3">
								<label>Password</label></br>
								<input type="password" name="password" placeholder="Enter Password">
							</div>
							<div class="form-group mb-3">
								<button type = "submit" name ="login_btn"class="btn btn-primary">Log-in </button>
							</div>
						</form>
						<button style="background-color: transparent; border:none; align-content:center" onclick="window.open('forgetPassword.php','_self')">Forget password ? </button>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
