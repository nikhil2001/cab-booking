<html>
<head>
    <title>ride ongoing</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
  </head>
<body>
<div style="text-align: center;font-size:2cm;"><span id="time"></span> </div>
    <div class="d-grid gap-2 col-4 mx-auto" style="padding-top: 30px; ">
      <button hidden class="btn btn-outline-dark" id='complete' onclick="window.open('thankyou.php','_self')">Destination arrived</button>
    </div>

<script>
const timerElement = document.getElementById('time');
let timer;

        function startTimeCountDown() {
            timer = 30;
            const timeCountdown = setInterval(countdown, 1000);
        }


        function countdown() {
          if(timer<11){
            document.getElementById("complete").hidden=false;
          }
            if (timer < 0) {
                clearTimeout(timer);
		timerElement.innerHTML = "ride ended";
		window.open("thankyou.php","_self");
            } else {
                timerElement.innerHTML = "ride ends in " + timer + " seconds" ;
                timer--;
            }
        }

        timerElement.addEventListener('click', ev => {
            startTimeCountDown();
        });
window.onload = function () {
    var fiveMinutes = 60 * 0.5,
        display = document.querySelector('#time');
    startTimeCountDown(fiveMinutes, display);
};
</script>
</body>
</html>