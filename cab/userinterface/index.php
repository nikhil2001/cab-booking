<?php
include('formconn.php');
session_start();
$userID = $_SESSION['email'];
?>


<html>
  <head>
    <title>Add Map</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
    <script type="module" src="./index.js" ></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.8.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.8.1/mapbox-gl.css' rel='stylesheet' />

  </head>
  <body>

  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script>
  <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css" type="text/css">


    <!-- navbar starts -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid" style="cursor: pointer;">
        <a class="navbar-brand" href="#">Nk's cab booking</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="window.open('previousrides.php','_self')">Previous rides</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
               Account
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#"><?= $userID ?></a></li>
                <form action="logout.php" method="post">
                  <li><button class="dropdown-item" type="submit" name="logoutBtn">Log-out</button></li>
                </form>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- navbar ends -->

  <!-- side bad  -->
  <div class="main__body">
    <div class="sidebar">
      <div class="sidebarOption">
        <form action="booked.php" method="POST" style="margin-left: 20px; padding-top:50%;">
          <h5 style="font-size: small;" id="from"> </h5>
          <input type="hidden" id="From" name="Fromloc">
          <input type="hidden" id="To" name="Toloc">
          <input type="hidden" id="fare" name="fare">
          <input type="hidden" id="long" name="long">
          <input type="hidden" id="lat" name="lat">
          <input type="hidden" id="longfrom" name="longfrom">
          <input type="hidden" id="latfrom" name="latfrom">
          <h5 style="font-size: small;padding-top:10px;" id="to"> </h5>
          <button type="submit" class="btn btn-dark" name="booked"  id="book" style="margin-top: 20px; margin-left:45px">Book</button>
        </form>
      </div>
  </div>
  <!-- side bar ends -->


  <div id='map' style='width: 100%; height: 550px;'></div>
  <script>
    mapboxgl.accessToken = 'pk.eyJ1IjoibmlraGlsYjAzIiwiYSI6ImNsMmU4ZDA4YTA0cjYzY3F6c3hhaW5oODcifQ.OX4BLeRlKfpt6MO7J0WzMA';

    navigator.geolocation.getCurrentPosition(successLocation, errorLocation, {
  enableHighAccuracy: true
})

function successLocation(position) {
  setupMap([position.coords.longitude, position.coords.latitude])
}

function errorLocation() {
  setupMap([-2.24, 53.48])
}

function setupMap(center) {
  const map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    center: center,
    zoom: 9
  })

  const nav = new mapboxgl.NavigationControl()
  map.addControl(nav)

  var directions = new MapboxDirections({
    accessToken: mapboxgl.accessToken
  })

map.addControl(
  new MapboxDirections({
  accessToken: mapboxgl.accessToken
  }),'top-left');
}
var from;
var to;
var clk = 0;
var fromlat;
var tolat;
var ans
document.body.addEventListener('click', function (e) {
    if (e.target instanceof HTMLTextAreaElement ||((e.target instanceof HTMLInputElement) && (e.target.getAttribute('type') === "text"))){
      clk++;
      if(clk%2==0){
        
        if(clk==2){
          from = e.target.value;  
          document.getElementById("From").value = from; 
          // console.log(document.getElementById("From").value );
          document.getElementById("from").innerHTML="<b>from address :</b> " + from;
        }
        if(clk==4){
          to = e.target.value;
          // $_SESSION['to']=to;
          document.getElementById("To").value = to;
          // console.log(document.getElementById("To").value)
          document.getElementById("to").innerHTML="<b>To address :</b>" + to;
        }
        if(clk===6){
          fromlat=e.target.value;
          console.log(fromlat);
        }
        if(clk===8){
          tolat=e.target.value;
          console.log(tolat);

          const ar1 = fromlat.split(",");
          var long1 = parseFloat(ar1[0]);
          var lat1 = parseFloat(ar1[1]);

          const ar2 = tolat.split(",");
          var long2 = parseFloat(ar2[0]);
          var lat2 = parseFloat(ar2[1]);
          console.log(lat2);
          console.log(long2);


          var p = 0.017453292519943295;    // Math.PI / 180
          var c = Math.cos;
          var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((long2 - long1) * p))/2;

          ans= 12742 * Math.asin(Math.sqrt(a));
          document.getElementById("fare").value = (ans*10);

          document.getElementById("lat").value=lat2;
          document.getElementById("long").value=long2;

          document.getElementById("latfrom").value=lat1;
          document.getElementById("longfrom").value=long1;
        }
      }
    }
});



  </script>

  </body>
</html>